import pandas as pd
import numpy as np
import sys
import glob

sys.path.insert(1, '../Utils')

import pickle
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import FormatStrFormatter

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import scipy as scipy
from scipy.optimize import curve_fit
from fit_functions import *
from helpers import *

import ROOT

matplotlib.rcParams.update(
    {
        'text.usetex': True,
        "font.family": "cursive",
        "font.sans-serif": ["Helvetica"]
        # 'mathtext.fontset': 'stix',
    }
)

matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'

# Define the pickle files that you want to plot.
pickleFiles = glob.glob("HLT*.pickle")

# Define the output path for the plots.
outputPath="/afs/cern.ch/work/p/pverschu/private/LundSecondment/responseAndResolutionStudies/eta_var/plots/"

y_min = 0.97
y_max = 1.01

# Loop over all the pickle files.
for pickleFilePath in pickleFiles:

    print("Plotting pickle file "+pickleFilePath)

    pos_ = [pos for pos, char in enumerate(pickleFilePath) if char == "_"]

    # Extract some info from the file name.
    HLT_name = pickleFilePath[pos_[0]+1:pos_[1]]
    HLT_bound = pickleFilePath[pos_[2]+1:pos_[3]]
    edge_type = pickleFilePath[pos_[1]+1:pos_[2]]
    
    # Define the lower pt response values that are excluded from the curve fit.
    # 0 means no excluded bins, 1 means the first, 2 means the first 2, etc. 
    colors_fit_func = ['g','c','b','orange','r','y','salmon','olive','darkviolet']
    fit_func_labels = [None]*len(colors_fit_func)
    data_labels = [None]*len(colors_fit_func)
    
    # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- 

    
    # Read the appropriate dataframe.
    df = pd.read_pickle(pickleFilePath)
    
    f, ax = plt.subplots(2)

    gs = gridspec.GridSpec(2, 1,height_ratios=[5,1])
    gs.update(wspace=0.05, hspace=0.05)              
    ax[0] = plt.subplot(gs[0])
    ax[1] = plt.subplot(gs[1])

    y_func_nom = None
    
    # Loop over the slices defined in the pickle file.
    for slice_name, i in zip(df.index, range(len(df.index))):

        print("Fitting responses of slice "+slice_name)
    
        x=df["x"].loc[slice_name]
        y=df["y"].loc[slice_name]

        x_error=df["xError"].loc[slice_name]
        y_error=df["yError"].loc[slice_name]
                
        fit_func_labels[i] = "Fitted polylog "+slice_name.replace("_", "")
        data_labels[i] = "GSC Responses "+slice_name.replace("_", "")

        x = np.array(x)
        y = np.array(y)
        x_error = np.array(x_error)
        y_error = np.array(y_error)

        x_min = x[0] - x_error[0]
        x_max = x[-1] + x_error[-1]

        plot_range = np.linspace(x_min,x_max,1000)

        #ax.text(800,1.6,"eta = "+eta_range)
        ax[0].errorbar(x, y, yerr=y_error, xerr=x_error,
                    linestyle='None',
                    marker="o",
                    color=colors_fit_func[i],
                    markersize=2,
                    linewidth=0.5,
                    label=data_labels[i],
                )
        popt, pcov = None, None

        try:
            popt, pcov = curve_fit(polyLog, x, y, sigma=y_error)
        except RuntimeError,e:
            print(e.message)
            continue

        y_func_ratio = None
        y_func = polyLog(plot_range, *popt)
        if i == 0:
            y_func_nom = y_func

        y_func_ratio = y_func / y_func_nom

        ax[0].plot(plot_range, y_func, color=colors_fit_func[i], label=fit_func_labels[i])
        ax[1].plot(plot_range, y_func_ratio, color=colors_fit_func[i])

        ax[0].set_xlim(x_min, x_max)
        ax[1].set_xlim(x_min, x_max)
        ax[0].set_xscale('log')
        ax[0].set_ylim(y_min, y_max)

    trigger_name = pickleFilePath[(pickleFilePath.rfind("j")-4):(pickleFilePath.rfind("j")+4)]

    try:
        int(trigger_name[-1])
    except ValueError:
        trigger_name = pickleFilePath[(pickleFilePath.rfind("j")-4):(pickleFilePath.rfind("j")+3)]

    # Get the ptavg boundaries defined in Utils/helpers.py
    ptAvgBoundaries = getHLTPtAvgBoundaries()
    HLTNames = getHLTPtNames()

    # Draw vertical lines for each stitching boundary.
    ax[0].vlines(ptAvgBoundaries, y_min, y_max, linewidth=1, linestyle='dashed', color='gray')

    # Draw text labels for each HLT
    for HLT_i in range(len(getHLTPtNames())):

        x_low = float(ptAvgBoundaries[HLT_i])
        x_up = float(ptAvgBoundaries[HLT_i+1])

        if HLTNames[HLT_i] == HLT_name:
            if edge_type == "lowedge":
                x_low = float(HLT_bound)
            else:
                x_up = float(HLT_bound)

        x_coord = (x_up - x_low)/2 + x_low

        ax[0].text(x_coord, 0.975, HLTNames[HLT_i], ha='center', fontsize=8)
    
    leg = ax[0].legend(borderpad=0.5, loc=1, ncol=2, frameon=True, prop={"size":10})#,framealpha=1)
    leg._legend_box.align = "left"

    ax[1].tick_params(axis='y', labelsize=8)
    ax[1].ticklabel_format(axis='y',useOffset=False)
    ax[1].set_ylim(0.998, 1.002)
        
    # Set log scale
    ax[1].set_xscale('log')

    ax[1].tick_params(axis='x', which='minor', labelsize = 12)
    ax[1].tick_params(axis='x', which='major', labelsize = 12)
    ax[1].tick_params(axis='y', labelsize=8)
    ax[1].xaxis.set_minor_formatter(FormatStrFormatter("%.0f"))
    ax[1].xaxis.set_major_formatter(FormatStrFormatter("%.0f"))

    ax[1].hlines(y=1.001, xmin=x_min, xmax=x_max, linewidth=1, linestyle='dashed', color='r')
    ax[1].hlines(y=0.999, xmin=x_min, xmax=x_max, linewidth=1, linestyle='dashed', color='r')

    # Set axis labels
    ax[0].set_ylabel("Online/Offline Response", fontsize=14, ha='right', y=0.7)    
    ax[0].get_xaxis().set_ticks([])
    
    ax[1].set_xlabel("$p_{T}^{avg}$", fontsize=14, ha='right',x=1.0)
    ax[1].set_ylabel(r'$\frac{\mbox{eta slice}}{\mbox{central slice}}$', fontsize=12, ha='right',y=0.8)
    #ax[1].set_ylabel(r'$\frac{\mbox{Polylog}}{\mbox{Response}}$', fontsize=8, ha='right',y=0.8)

    # Add grid
    ax[0].grid(True)


    pdf_path = outputPath+"response_fitted"+"_"+pickleFilePath[(pickleFilePath.rfind("/")+1):-7]+".pdf"        


    # Save plot as .pdf
    f.savefig(pdf_path)

