import numpy as np

# Define polylogarithms
def polyLog(x,a4,a3,a2,a1,am1,am2,a0):
    return a4*np.log(x)**4 + a3*np.log(x)**3 + a2*np.log(x)**2 + a1*np.log(x)**1 + a0*np.log(x)**0 + am1*np.log(x)**(-1) + am2*np.log(x)**-2

def polyLog_2(x,a3,a2,a1,am1,am2,a0):
    return a3*np.log(x)**3 + a2*np.log(x)**2 + a1*np.log(x)**1 + a0*np.log(x)**0 + am1*np.log(x)**(-1) + am2*np.log(x)**-2

def polyLog_3(x,a2,a1,am1,am2,a0):
    return a2*np.log(x)**2 + a1*np.log(x)**1 + a0*np.log(x)**0 + am1*np.log(x)**(-1) + am2*np.log(x)**-2

def polyLog_4(x,a5,a4,a3,a2,a1,am1,am2,a0):
    return a5*np.log(x)**5 + a4*np.log(x)**4 + a3*np.log(x)**3 + a2*np.log(x)**2 + a1*np.log(x)**1 + a0*np.log(x)**0 + am1*np.log(x)**(-1) + am2*np.log(x)**-2

def polyLog_5(x,a4,a3,a2,a1,am1,am2,am3,a0):
    return a4*np.log(x)**4 + a3*np.log(x)**3 + a2*np.log(x)**2 + a1*np.log(x)**1 + a0*np.log(x)**0 + am1*np.log(x)**(-1) + am2*np.log(x)**-2 + am3*np.log(x)**-3

def polyLog_6(x,a4,a3,a2,a1,am1,a0):
    return a4*np.log(x)**4 + a3*np.log(x)**3 + a2*np.log(x)**2 + a1*np.log(x)**1 + a0*np.log(x)**0 + am1*np.log(x)**(-1)

def polyFive(x,a4,a3,a2,a1,a0):
    return a4 * x**4 + a3 * x**3 + a2 * x**2 + a1 * x + a0


# Define the fit start values of polylogarithms
def p0():
    return [ -2.08860232e+01,   6.26727397e+02,  -7.82315446e+03,   5.19976331e+04, 3.85797124e+05,  -3.19016899e+05,  -1.94094224e+05]

def p0_2():
    return [ 6.26727397e+02,  -7.82315446e+03,   5.19976331e+04, 3.85797124e+05,  -3.19016899e+05,  -1.94094224e+05]

def p0_3():
    return [ -7.82315446e+03,   5.19976331e+04, 3.85797124e+05,  -3.19016899e+05,  -1.94094224e+05]

def p0_4():
    return [ 5.54245807e+01,  -2.08860232e+01,   6.26727397e+02,  -7.82315446e+03,   5.19976331e+04, 3.85797124e+05,  -3.19016899e+05,  -1.94094224e+05]

def p0_5():
    return [  2.11023006e+02,  -7.68932753e+03,   1.19861495e+05,  -1.03614895e+06, -1.66374823e+07,   2.86163334e+07,  -2.10591916e+07,   5.36480157e+06]

def p0_6():
    return [  2.11023006e+02,  -7.68932753e+03,   1.19861495e+05,  -1.03614895e+06, -1.66374823e+07,   2.86163334e+07]
