import ROOT

import math
from array import array
from JES_BalanceFitter import JES_BalanceFitter
from fit_functions import *
import pandas as pd

def getHLTPtAvgBoundaries():
    return [67, 84, 182, 227, 316, 394, 954]

def getHLTPtNames():
    return ["j45", "j60", "j110", "j175", "j260", "j360"]

def getHLTPtAvgRange(HLT):

    if HLT == "HLT_j360":
        return [400,1400]
    elif HLT == "HLT_j15":
        return [25,40]
    elif HLT == "HLT_j25":
        return [40,55]
    elif HLT == "HLT_j35":
        return [55,65]
    elif HLT == "HLT_j45":
        return [65,85]
    elif HLT == "HLT_j60":
        return [85,175]
    elif HLT == "HLT_j110":
        return [175,220]
    elif HLT == "HLT_j175":
        return [220,330]
    elif HLT == "HLT_j260":
        return [330,400]
    else:
        print("Didnt find trigger!")
        exit(0)

def getXRange(pickleFile, param_name):

    # Read the appropriate dataframe.
    df = pd.read_pickle(pickleFile)

    x=df["x"].loc[param_name]
    x_error=df["xError"].loc[param_name]    
    
    x_min = x[0] - 2*x_error[0]
    x_max = x[-1] + 2*x_error[-1]

    return [x_min, x_max]


def findPossibleBins(boundaries,hist):

    pos_boundaries = []

    for edge_i in range(len(boundaries)):
        
        poss_bins = []

        edge = boundaries[edge_i]
        
        bin_i = hist.GetYaxis().FindBin(edge)
        
        if edge >= hist.GetYaxis().GetBinCenter(bin_i):
            bin_i = bin_i + 1

        pos_boundaries.append(bin_i)

    pos_boundaries_vars = []

    for pos_boundary_i in range(len(pos_boundaries)):

        pos_boundaries[pos_boundary_i] = pos_boundaries[pos_boundary_i] - 1

        pos_boundaries_vars.append(list(pos_boundaries))

        pos_boundaries[pos_boundary_i] = pos_boundaries[pos_boundary_i] + 2

        pos_boundaries_vars.append(list(pos_boundaries))

        pos_boundaries[pos_boundary_i] = pos_boundaries[pos_boundary_i] - 1

        pos_boundaries_vars.append(list(pos_boundaries))

    
    return pos_boundaries_vars

def getBinEdges(bin_lists, hist):

    bin_edges = []

    for bin_list in bin_lists:

        bin_edges_i = []

        for bin_i in bin_list:
            bin_edges_i.append(hist.GetYaxis().GetBinLowEdge(bin_i))

        bin_edges.append(bin_edges_i)

    return bin_edges


def symmetrize_eta(h3D):

    central_z_bin = h3D.GetZaxis().FindBin(0)
    final_z_bin = h3D.GetNbinsZ()+1

    n_new_z_bins = h3D.GetNbinsZ()/2
    
    x_bin_edges = []
    y_bin_edges = []
    z_bin_edges = []

    for i in range(h3D.GetNbinsX()+1):
        x_bin_edges.append(h3D.GetXaxis().GetBinLowEdge(i+1))

    for i in range(h3D.GetNbinsY()+1):
        y_bin_edges.append(h3D.GetYaxis().GetBinLowEdge(i+1))

    for i in range(central_z_bin, final_z_bin+1):
        z_bin_edges.append(h3D.GetZaxis().GetBinLowEdge(i))

    hist_name = h3D.GetName()+"_symm"

    sym_h3D = ROOT.TH3D(hist_name, hist_name, len(x_bin_edges)-1, array('d',x_bin_edges),  len(y_bin_edges)-1, array('d',y_bin_edges),  len(z_bin_edges)-1, array('d',z_bin_edges))

    for i in range(sym_h3D.GetNbinsX()):
        for j in range(sym_h3D.GetNbinsY()):
            for k in range(central_z_bin, final_z_bin):
                
                z_mirr_bin = 2*central_z_bin - k - 1

                right = h3D.GetBinContent(i+1, j+1, k)
                left = h3D.GetBinContent(i+1, j+1, z_mirr_bin)

                sym_h3D.SetBinContent(i+1, j+1, k - central_z_bin + 1, right + left)
    
    return sym_h3D
        
def find_nearest_idx(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx


def plot_hist_fit(h1d, fit, name):

    c1 = ROOT.TCanvas("","",400,300)

    h1d.Draw("HIST")

    fit.Draw("SAME")
    
    name = "plots/"+name+".pdf"

    c1.SaveAs(name)

def get_list_hist_names(inFile, name_filters):

    hist_name_list = []
    
    # Get all the object keys in the root-file.
    for key in inFile.GetListOfKeys():
        
        # Get the object names.
        hist_name = key.GetName()
        
        include_hist = True

        for name in name_filters:
            if not name in hist_name:
                include_hist = False
                break
            continue
        
        if include_hist:
            hist_name_list.append(hist_name)

    return hist_name_list            

def get_bin_edges(hist, axis):

    bin_edges = []
    
    if axis == "x":
        for bin_i in range(hist.GetNbinsX()+1):
            bin_edges.append(hist.GetXaxis().GetBinLowEdge(bin_i+1))
    elif axis == "y":
        for bin_i in range(hist.GetNbinsY()+1):
            bin_edges.append(hist.GetYaxis().GetBinLowEdge(bin_i+1))
    elif axis == "z":
        for bin_i in range(hist.GetNbinsZ()+1):
            bin_edges.append(hist.GetZaxis().GetBinLowEdge(bin_i+1))
    else: 
        print("Axis not recognized! ")
    
    return bin_edges
    

def adjust_bin_slices(h3D, zslices):

    new_zslices = np.zeros(zslices.shape)

    for z_i in range(len(zslices)):
        z_point = zslices[z_i]
        z_bin = h3D.GetZaxis().FindBin(z_point)
        if z_point < h3D.GetZaxis().GetBinCenter(z_bin):
            new_zslices[z_i] = h3D.GetZaxis().GetBinLowEdge(z_bin)
        else:
            new_zslices[z_i] = h3D.GetZaxis().GetBinUpEdge(z_bin)
    return new_zslices

def get_bin_edges_entries_errors(h1D):

    binEdges = []
    binEntries = []
    binErrors = []

    # Loop over the response bins in the 1D histogram
    for i in range(1, h1D.GetNbinsX()+1):# Plus one to include last bin
        binEdges.append(h1D.GetXaxis().GetBinLowEdge(i)) # Get the current bin edge for plotting later
        binEntries.append(h1D.GetBinContent(i)) # Get the number of entries in the bin for plotting later
        binErrors.append(h1D.GetBinError(i)) # Get the bin error to get the width of the bin... For plotting later
    binEdges.append(h1D.GetXaxis().GetBinUpEdge(h1D.GetNbinsX()))# Append the right most edge of the last bin

    return binEdges, binEntries, binErrors

def fill_dataframe(df, slice_name, h2D, fit, currentRebinnedBin, fitMin, fitMax, Chi2Ndof, binEdges, binEntries, binErrors):


    df["x"].loc[slice_name].append(float(h2D.GetXaxis().GetBinCenter(currentRebinnedBin)))
    df["y"].loc[slice_name].append(float(fit.GetParameter(1)))
    df["xError"].loc[slice_name].append(float((h2D.GetXaxis().GetBinWidth(currentRebinnedBin)/2.0)))#half bin width
    df["yError"].loc[slice_name].append(float(fit.GetParError(1)))
    df["sigma"].loc[slice_name].append(float(fit.GetParameter(2)))
    df["sigmaError"].loc[slice_name].append(float(fit.GetParError(2)))
    try:
        df["sigmaOverY"].loc[slice_name].append(float(fit.GetParameter(2) / float(fit.GetParameter(1))))
        df["sigmaOverYError"].loc[slice_name].append(np.sqrt((fit.GetParError(2)/fit.GetParameter(2))**2+(fit.GetParError(1)/fit.GetParameter(1))**2))
    except:
        df["sigmaOverY"].loc[slice_name].append(0)
        df["sigmaOverYError"].loc[slice_name].append(0)
                
    # Append the lists of 1D histogram info to the list of lists in the dataframe data series
    df["TH1BinEdges"].loc[slice_name].append(binEdges)
    df["TH1BinEntries"].loc[slice_name].append(binEntries)
    df["TH1BinErrors"].loc[slice_name].append(binErrors)
    
    df["gaussianParameters"].loc[slice_name].append([fit.GetParameter(0),fit.GetParameter(1),fit.GetParameter(2), fitMin, fitMax, Chi2Ndof])

    return df

def get_1D_slice(inh2D, currentSlice, currentRebinnedBin):

    h2D = inh2D.Clone()

    # Name of the 1D projection
    projName = "slice"+str(currentSlice[0])+"to"+str(currentSlice[1])+"_projectionBin"+str(h2D.GetXaxis().GetBinLowEdge(currentRebinnedBin))+"to"+str(h2D.GetXaxis().GetBinUpEdge(currentRebinnedBin))

    # Project the current 2D histogram bin into a 1D histogram
    h1D=h2D.ProjectionY(projName, currentRebinnedBin, currentRebinnedBin)
    
    return h1D

def print_entries(h1D):

    for i in range(h1D.GetNbinsX()):
        print(h1D.GetBinContent(i+1))

def print_entries_3D(h3D):
    for i in range(h3D.GetNbinsX()):
        for j in range(h3D.GetNbinsY()):
            for k in range(h3D.GetNbinsZ()):
                entries= h3D.GetBinContent(i+1, j+1, k+1)
                if entries > 0:
                    print(entries)


def print_bins_2D(h2D):

    x_bins = [None]*h2D.GetNbinsX()
    y_bins = [None]*h2D.GetNbinsY()

    for i in range(len(x_bins)):
        x_bins[i] = h2D.GetXaxis().GetBinLowEdge(i+1)

    for j in range(len(y_bins)):
        y_bins[j] = h2D.GetYaxis().GetBinLowEdge(j+1)

    print("X bin edges: ")
    print(x_bins)
    print("Y bin edges: ")
    print(y_bins)

def print_ranges(h):

    print("X range: ["+str(h.GetXaxis().GetXmin())+", "+str(h.GetXaxis().GetXmax())+"]")
    print("Y range: "+str(h.GetYaxis().GetXmin())+", "+str(h.GetYaxis().GetXmax())+"]")
    print("Z range: "+str(h.GetZaxis().GetXmin())+", "+str(h.GetZaxis().GetXmax())+"]")
    
    

def print_bins_3D(h3D):

    x_bins = [None]*h3D.GetNbinsX()
    y_bins = [None]*h3D.GetNbinsY()
    z_bins = [None]*h3D.GetNbinsZ()

    for i in range(len(x_bins)):
        x_bins[i] = h3D.GetXaxis().GetBinLowEdge(i+1)

    for j in range(len(y_bins)):
        y_bins[j] = h3D.GetYaxis().GetBinLowEdge(j+1)

    for k in range(len(z_bins)):
        z_bins[k] = h3D.GetZaxis().GetBinLowEdge(k+1)

    print("X bin edges: ")
    print(x_bins)
    print("Y bin edges: ")
    print(y_bins)
    print("Z bin edges: ")
    print(z_bins)
    
def get_2D_slice_new(h3D, currentSlice, slicingAxis, responseAxis, projectionAxis, projectionRebinWidth):
    
    h3D = h3D.Clone() # Clone it ot keep it intact

    if slicingAxis == "y":
        h3D.GetYaxis().SetRangeUser(currentSlice[0], currentSlice[1])     
    elif slicingAxis == "z":
        h3D.GetZaxis().SetRangeUser(currentSlice[0], currentSlice[1]) 

    # Project the 3D histogram with the sliced axis range into a 2D histogram
    h2D=h3D.Project3D(responseAxis + projectionAxis)

    # Rebin the 2D histogram x-axis according to the desired rebinningFactor
    h2D.RebinX(projectionRebinWidth)

    return h2D


def get_2D_slice(inFile, TH3Name, currentSlice, slicingAxis, responseAxis, projectionAxis, projectionRebinWidth):

    inTH3 = inFile.Get(TH3Name) # Retreive the current 3D histogram form the root file
    h3D = inTH3.Clone() # Clone it ot keep it intact

    # Set the 3D histogram range to correspond to the desired slice range
    if slicingAxis == "y":
        h3D.GetYaxis().SetRangeUser(currentSlice[0], currentSlice[1])     
    elif slicingAxis == "z":
        h3D.GetZaxis().SetRangeUser(currentSlice[0], currentSlice[1]) 

    # Project the 3D histogram with the sliced axis range into a 2D histogram
    h2D=h3D.Project3D(responseAxis + projectionAxis)

    # Rebin the 2D histogram x-axis according to the desired rebinningFactor
    h2D.RebinX(projectionRebinWidth)

    return h2D

def get_dict_list_dataperiods(data_periods_files):

    dictionaryList=[] # Initialize an empty list to house the dictionaries, one per slice

    for data_period in data_periods_files:

        dictionaryList.append(pd.Series({
            "x"             :[],
            "y"             :[],
            "xError"        :[],
            "yError"        :[],
            "sigma"         :[],
            "sigmaError"    :[],
            "sigmaOverY"    :[],
            "sigmaOverYError"    :[],
            "fitAmplitude"  :[],
            "fitMin"        :[],
            "fitMax"        :[],
            
            "TH1BinEdges"   :[],
            "TH1BinEntries" :[],
            "TH1BinErrors"  :[],
            "gaussianParameters" : [],
        },
                                        name=data_period))
        
    return dictionaryList

def get_dict_list_rebinfactors(rebinFactors):

    dictionaryList=[] # Initialize an empty list to house the dictionaries, one per slice

    for rebinFactor in rebinFactors:

        dictionaryList.append(pd.Series({
            "x"             :[],
            "y"             :[],
            "xError"        :[],
            "yError"        :[],
            "sigma"         :[],
            "sigmaError"    :[],
            "sigmaOverY"    :[],
            "sigmaOverYError"    :[],
            "fitAmplitude"  :[],
            "fitMin"        :[],
            "fitMax"        :[],
            
            "TH1BinEdges"   :[],
            "TH1BinEntries" :[],
            "TH1BinErrors"  :[],
            "gaussianParameters" : [],
        },
                                        name="rebinFactor_"+str(rebinFactor)))
        
    return dictionaryList
    

def get_dict_list_eta(TH3Name, zslices):

    dictionaryList=[] # Initialize an empty list to house the dictionaries, one per slice

    for z_i in range(len(zslices)-1):
        dict_name = "eta_["+str(round(zslices[z_i],4))+" "+str(round(zslices[z_i+1],4))+"]"
        dictionaryList.append(pd.Series({
            "x"             :[],
            "y"             :[],
            "xError"        :[],
            "yError"        :[],
            "sigma"         :[],
            "sigmaError"    :[],
            "sigmaOverY"    :[],
            "sigmaOverYError"    :[],
            "fitAmplitude"  :[],
            "fitMin"        :[],
            "fitMax"        :[],
            
            "TH1BinEdges"   :[],
            "TH1BinEntries" :[],
            "TH1BinErrors"  :[],
            "gaussianParameters" : [],
        },
                                        name=dict_name))
        
        
    return dictionaryList

def get_dict_list_fitparams(TH3Name, param_name, param_vals):

    dictionaryList=[] # Initialize an empty list to house the dictionaries, one per slice

    for i in range(len(param_vals)):
        dict_name = param_name+"_"+str(param_vals[i])
        dictionaryList.append(pd.Series({
            "x"             :[],
            "y"             :[],
            "xError"        :[],
            "yError"        :[],
            "sigma"         :[],
            "sigmaError"    :[],
            "sigmaOverY"    :[],
            "sigmaOverYError"    :[],
            "fitAmplitude"  :[],
            "fitMin"        :[],
            "fitMax"        :[],
            
            "TH1BinEdges"   :[],
            "TH1BinEntries" :[],
            "TH1BinErrors"  :[],
            "gaussianParameters" : [],
        },
                                        name=dict_name))
        
        
    return dictionaryList

