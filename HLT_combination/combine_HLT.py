

import pandas as pd
import numpy as np
import sys

sys.path.insert(1, '../Utils')

import pickle
from matplotlib.backends.backend_pdf import PdfPages
from JES_BalanceFitter import JES_BalanceFitter
from helpers import *

import ROOT


# -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- 

path="/afs/cern.ch/work/p/pverschu/private/LundSecondment/responseAndResolutionStudies"

# Define the list with root-files containing the 3D histograms.
listOfRootFilePaths = ["/afs/cern.ch/work/p/pverschu/private/LundSecondment/responseAndResolutionStudies/data/v30/merged.root"
            ]

trigger_list = ["j45","j60","j110","j175","j260","j360"]

boundaries = [65, 85, 175, 220, 330, 400, 1000]

zvar = "etaLeading"

# Define the z-axis of the 3D histogram. 
# Can be etaLeading or etaSubLeading referring
# to the eta of the leading or subleading jet.
slicingAxis = "z"

# Define the y-axis. This is always PtAvgLeading.
yvar = "ptAvg"
projectionAxis = "y"

# The axis with the online/offline responses.
responseAxis = "x"

# Define the eta range and slices.
zslices = np.linspace(-2.4,2.4,2)

# Define some words that need to occur in the name of histograms in the root-files.
name_filters = ["GSC",zvar,yvar]

inFile = ROOT.TFile(listOfRootFilePaths[0])

hist_list = []

for trigger in trigger_list:

    name_filters.append("HLT_"+trigger)
    hist_name = get_list_hist_names(inFile, name_filters)
    name_filters.pop()

    h3D = inFile.Get(hist_name[0])
    h3D.SetDirectory(0)
    
    hist_list.append(h3D)

# Gets the bin indices on the HLT ptavg boundaries.
bin_lists = findPossibleBins(boundaries, hist_list[-1])

# Gets the bin edges on the HLT ptavg boundaries.
bin_edges_lists = getBinEdges(bin_lists, hist_list[-1])

trigger_i = -1

# Define the output path of the root-file containing the combined 3D histogram.
outputFilePath = "/afs/cern.ch/work/p/pverschu/private/LundSecondment/responseAndResolutionStudies/data/HLT_combined/merged_binvars_45to360.root"

outputFile = ROOT.TFile(outputFilePath,"RECREATE")

for bin_list_i in range(len(bin_lists)):

    if (bin_list_i%3) == 0:
        trigger_i += 1

    if trigger_i < len(trigger_list):
        hist_name = "HLT_"+trigger_list[trigger_i]+"_lowedge_"+str(int(bin_edges_lists[bin_list_i][trigger_i]))
    else:
        hist_name = "HLT_"+trigger_list[trigger_i-1]+"_upedge_"+str(int(bin_edges_lists[bin_list_i][trigger_i]))

    bin_list = bin_lists[bin_list_i]

    nr_z_bins = bin_list[-1] - bin_list[0]

    x_edges = get_bin_edges(hist_list[-1], "x")
    y_edges = get_bin_edges(hist_list[-1], "y")[bin_list[0]-1:bin_list[0]+nr_z_bins]
    z_edges = get_bin_edges(hist_list[-1], "z")

    h3D_combined = ROOT.TH3D(hist_name,hist_name,len(x_edges)-1, array('d',x_edges), len(y_edges)-1, array('d',y_edges), len(z_edges)-1, array('d',z_edges))
    
    cur_hist = None

    for i in range(h3D_combined.GetNbinsX()):
        for j in range(h3D_combined.GetNbinsZ()):
            z_count = 1
            trigger_count = 0

            for k in range(hist_list[-1].GetNbinsY()):

                if (k+1) >= bin_list[trigger_count] and trigger_count < len(trigger_list):
                    cur_hist = hist_list[trigger_count]
                    trigger_count += 1

                if (k+1) >= bin_list[0] and (k+1) < bin_list[-1]:
                    h3D_combined.SetBinContent(i+1, z_count, j+1, cur_hist.GetBinContent(i+1, k+1, j+1))
                    z_count += 1

    h3D_combined.Write()

# Save the 3D histogram to a root-file.
outputFile.Close()

# -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- 
