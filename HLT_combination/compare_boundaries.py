
import glob
import pandas as pd
import numpy as np
import sys

sys.path.insert(1, '../Utils')

import pickle
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import FormatStrFormatter

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import scipy as scipy
from scipy.optimize import curve_fit
from fit_functions import *
from helpers import *

import ROOT


matplotlib.rcParams.update(
    {
        'text.usetex': True,
        "font.family": "cursive",
        "font.sans-serif": ["Helvetica"]
        # 'mathtext.fontset': 'stix',
    }
)

matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'

nom_pickle = "pickles/merged_binvars_45to360_HLT_j360_upedge_954_nSigmas_[1.0].pickle"
up_pickle = "pickles/merged_binvars_45to360_HLT_j360_upedge_1065_nSigmas_[1.0].pickle"
down_pickle = "pickles/merged_binvars_45to360_HLT_j360_upedge_854_nSigmas_[1.0].pickle"


param_name="nSigma_1.0"


 
func_list = [polyLog]#, polyLog_2, polyLog_3, polyLog_4]
#func_list = [polyLog, polyLog_5, polyLog_6]

#init_parm = [p0(),p0_2(), p0_3(), p0_4()]
init_parm = [p0()]#,p0_5(), p0_6()]

#func_list_labels = ["[-2,4]","[-1,4]","[-3,4]"]

pickleFiles = [nom_pickle, down_pickle, up_pickle]

x_min = getXRange(down_pickle, param_name)[0]
x_max = getXRange(up_pickle, param_name)[1]

colors_fit_func = ['g','c','b','orange','r','y']
fit_func_labels = [None]*len(colors_fit_func)
data_labels = [None]*len(colors_fit_func)

y_min = 0.97
y_max = 1.01

y_func_ratio = None

f, ax = plt.subplots(2)

gs = gridspec.GridSpec(2, 1,height_ratios=[5,1])
gs.update(wspace=0.05, hspace=0.05)              
ax[0] = plt.subplot(gs[0])
ax[1] = plt.subplot(gs[1])

ptAvgBoundaries = getHLTPtAvgBoundaries()
HLTNames = getHLTPtNames()

ax[0].vlines(ptAvgBoundaries, y_min, y_max, linewidth=1, linestyle='dashed', color='gray')


for pickle_i in range(len(pickleFiles)):
    
    pickleFilePath = pickleFiles[pickle_i]

    print("Plotting pickle file "+pickleFilePath)
    
    outputPath="/afs/cern.ch/work/p/pverschu/private/LundSecondment/responseAndResolutionStudies/HLT_combination/plots/"

    # Define the lower pt response values that are excluded from the curve fit.
    # 0 means no excluded bins, 1 means the first, 2 means the first 2, etc. 
    # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- 
    
    
    # Read the appropriate dataframe.
    df = pd.read_pickle(pickleFilePath)

    pos_ = [pos for pos, char in enumerate(pickleFilePath) if char == "_"]
    
    HLT_name = pickleFilePath[pos_[3]+1:pos_[4]]
    HLT_bound = pickleFilePath[pos_[5]+1:pos_[6]]
    edge_type = pickleFilePath[pos_[4]+1:pos_[5]]

    fit_func_labels[pickle_i] = "Fitted polylog "+HLT_name+" "+HLT_bound
    data_labels[pickle_i] = "GSC Responses "+HLT_name+" "+HLT_bound

    x=df["x"].loc[param_name]
    y=df["y"].loc[param_name]

    x_error=df["xError"].loc[param_name]
    y_error=df["yError"].loc[param_name]
    
    x = np.array(x)
    y = np.array(y)
    x_error=np.array(x_error)
    y_error=np.array(y_error)

    plot_range = np.linspace(x_min,x_max,1000)

    print("Fitting responses to polylog")

    popt, pcov = curve_fit(polyLog, x, y, sigma=y_error)#, p0=init_parm[i])
                
    y_func = polyLog(plot_range, *popt)
 
    if pickle_i == 0:
        y_func_nom = y_func


    ax[0].vlines(float(HLT_bound), y_min, y_max, linewidth=1, linestyle='dashed', color=colors_fit_func[pickle_i])

    y_func_ratio = y_func / y_func_nom

    ax[0].errorbar(x, y, yerr=y_error, xerr=x_error,
                   linestyle='None',
                   marker="o",
                   color=colors_fit_func[pickle_i],
                   markersize=2,
                   linewidth=0.5,
                   label=data_labels[pickle_i],
               )

    ax[0].plot(plot_range, y_func, color=colors_fit_func[pickle_i], label=fit_func_labels[pickle_i])
    ax[1].plot(plot_range, y_func_ratio, color=colors_fit_func[pickle_i])
    
    ax[0].set_xlim(x_min, x_max)
    ax[1].set_xlim(x_min, x_max)
    ax[0].set_xscale('log')
    ax[0].set_ylim(y_min, y_max)

for HLT_i in range(len(getHLTPtNames())):
    
    x_low = float(ptAvgBoundaries[HLT_i])
    x_up = float(ptAvgBoundaries[HLT_i+1])
    
    if HLTNames[HLT_i] == HLT_name:
        if edge_type == "lowedge":
            x_low = float(HLT_bound)
        else:
            x_up = float(HLT_bound)
            
    x_coord = (x_up - x_low)/2 + x_low
            
    ax[0].text(x_coord, 0.975, HLTNames[HLT_i], ha='center', fontsize=8)
    
    
leg = ax[0].legend(borderpad=0.5, loc=1, ncol=2, frameon=True, prop={"size":10})#,framealpha=1)
leg._legend_box.align = "left"
#leg.set_title(plotDict[xAxisVariable]["legendTitle"])

ax[1].tick_params(axis='y', labelsize=8)
ax[1].ticklabel_format(axis='y',useOffset=False)
ax[1].set_ylim(0.998, 1.002)

# Set log scale
ax[1].set_xscale('log')

ax[1].tick_params(axis='x', which='minor', labelsize = 12)
ax[1].tick_params(axis='x', which='major', labelsize = 12)
ax[1].tick_params(axis='y', labelsize=8)
ax[1].xaxis.set_minor_formatter(FormatStrFormatter("%.0f"))
ax[1].xaxis.set_major_formatter(FormatStrFormatter("%.0f"))

ax[1].hlines(y=1.001, xmin=x_min, xmax=x_max, linewidth=1, linestyle='dashed', color='r')
ax[1].hlines(y=0.999, xmin=x_min, xmax=x_max, linewidth=1, linestyle='dashed', color='r')

# Set axis labels
ax[0].set_ylabel("Online/Offline Response", fontsize=14, ha='right', y=0.7)    
ax[0].get_xaxis().set_ticks([])

ax[1].set_xlabel("$p_{T}^{avg}$", fontsize=14, ha='right',x=1.0)
ax[1].set_ylabel(r'$\frac{\mbox{bound var}}{\mbox{nom}}$', fontsize=12, ha='right',y=0.8)
#ax[1].set_ylabel(r'$\frac{\mbox{Polylog}}{\mbox{Response}}$', fontsize=8, ha='right',y=0.8)

# Add grid
ax[0].grid(True)

pdf_path = outputPath+"response_fitted"+"_"+nom_pickle[nom_pickle.rfind("/")+1:-6]+"pdf"
    
# Save plot as .pdf
f.savefig(pdf_path)

