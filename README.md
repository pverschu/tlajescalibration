# TLAJESCalibration

This repository contains python scripts that takes 3D histograms filled with dijet events and derives online/offline response correction factors.

# Input

The 3D histograms dimensions:
- x = online/offline response
- y = pt average = (pt_lead - pt_sublead)/(pt_lead + pt_sublead)
- z = eta (sub)leading jet

# Process

- Create 1D slices for each pt average bin.
- Fit a Gaussian to the histogram of each slice.
- Get the Gaussian mean parameter and the parameter error from the fit results.
- Plot this mean and its error for each pt average bin.
- Fit a polylogarithmic function to these means.
- Define a new histogram with different pt average binning
- Set the number of entries for the new binning to 1/polylogarithmic function with the function evaluated at the center of each pt average bin.

# Output

The output is a 1D histogram that can be used in ATLAS analysis software.

# How to run
