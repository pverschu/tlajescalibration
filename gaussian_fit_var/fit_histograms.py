

import pandas as pd
import numpy as np
import sys

sys.path.insert(1, '../Utils')

import pickle
from matplotlib.backends.backend_pdf import PdfPages
from JES_BalanceFitter import JES_BalanceFitter
from fit_functions import *
from helpers import *

import ROOT


# -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- 

path="/afs/cern.ch/work/p/pverschu/private/LundSecondment/responseAndResolutionStudies"

# Define the list with root-files containing the 3D histograms.
listOfRootFilePaths = [path+"/data/v30/merged.root",
            ]

zvar = "etaLeading"
slicingAxis = "z"

yvar = "ptAvg"
projectionAxis = "y"

responseAxis = "x"

# (zmin, zmax, number of slices + 1)
zslices = np.linspace(-2.8,2.8,2)

projectionRebinWidth = 1

nSigmas = [1.0, 1.1, 1.2, 1.3]

fitOptString = "RESQ"

# Define some words that need to occur in the name of histograms in the root-files.
name_filters = ["GSC",yvar,zvar]

# -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- 



# Loop over the file paths
for rootFilePath in listOfRootFilePaths:

    print("Analyzing file "+rootFilePath)

    # Open root file containing 3D histograms
    inFile = ROOT.TFile.Open(rootFilePath) 

    # Get the histograms from the root-file that have all the 
    # key words in their object name.
    hist_names = get_list_hist_names(inFile, name_filters)

    # Loop over the histograms.
    for hist_name in hist_names:

        h3D = inFile.Get(hist_name)

        # The chosen slices might not align with the binning.
        # To avoid double counting bins one needs to adjust
        # the slice ranges to align with bin edges.
        zslices = adjust_bin_slices(h3D, zslices)

        # Create a dictionary list from the key names in the ROOT file.
        dictionaryList = get_dict_list_fitparams(hist_name, "nSigma", nSigmas)        

        # Create the dataframe form the dictionary list.
        df = pd.DataFrame(dictionaryList)

        # Define a path and name for the dataframe
        dfPath = "pickles/"+hist_name+"_nSigmas_"+str(nSigmas)

        for nSigmaForFit in nSigmas:
        
            print("Fit nSigma "+str(nSigmaForFit))

            nSigma_name = "nSigma_"+str(nSigmaForFit)

            # Create JESBfitter object and set the fit options.
            JESBfitter = JES_BalanceFitter(nSigmaForFit)
            JESBfitter.SetGaus()
            JESBfitter.SetFitOpt(fitOptString)
            
            currentSlice = [zslices[0], zslices[1]]
            
            # Get the 2D slice of the 3D histogram.
            h2D = get_2D_slice(inFile, hist_name, currentSlice, slicingAxis, responseAxis, projectionAxis, projectionRebinWidth)

            # Loop over the bins in the 2D histogram x-axis
            for currentRebinnedBin in range(1, h2D.GetNbinsX()+1): # Histograms start at bin 1, plus one to include last bin

                h1D = get_1D_slice(h2D, currentSlice, currentRebinnedBin)

                # If the current 2D bin is empty skip it
                if h1D.GetEntries() == 0:
                    #print("empty 1D hist, skipping!")
                    continue                

                # Set the fit limits based on the 1D histogram properties
                fitMax = h1D.GetMean() + nSigmaForFit * h1D.GetRMS()
                fitMin = h1D.GetMean() - nSigmaForFit * h1D.GetRMS()
                
                # Obtain fit using JES_BalanceFitter           
                JESBfitter.Fit(h1D, fitMin, fitMax)
                fit = JESBfitter.GetFit()
                histFit = JESBfitter.GetHisto()
                Chi2Ndof = JESBfitter.GetChi2Ndof()

                # Initialize empty lists to hold the values of each 1D histogram.
                # There will be as many 1D histograms as Y axis bins of the 3D histogram
                # Get all the bin edges, entries and errors in python lists.
                binEdges, binEntries, binErrors = get_bin_edges_entries_errors(h1D)    

                df = fill_dataframe(df, nSigma_name, h2D, fit, currentRebinnedBin, fitMin, fitMax, Chi2Ndof, binEdges, binEntries, binErrors)
        # Save the dataframe to disk as pickle. One dataframe per slice per root file path
        df.to_pickle(dfPath+".pickle") 
        del df

    inFile.Close() # Close the root file



# -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- 
