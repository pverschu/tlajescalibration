
import glob
import pandas as pd
import numpy as np
import sys

sys.path.insert(1, '../Utils')

import pickle
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import FormatStrFormatter

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import scipy as scipy
from scipy.optimize import curve_fit
from fit_functions import *
from helpers import *

import ROOT

matplotlib.rcParams.update(
    {
        'text.usetex': True,
        "font.family": "cursive",
        "font.sans-serif": ["Helvetica"]
        # 'mathtext.fontset': 'stix',
    }
)

matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'

pickleFiles = glob.glob('pickles/*pickle')

for pickleFilePath in pickleFiles:

    print("Plotting pickle file "+pickleFilePath)

    outputPath="/afs/cern.ch/work/p/pverschu/private/LundSecondment/responseAndResolutionStudies/gaussian_fit_var/plots/"

    n_rem=1

    # if ("HLT_j260" in pickleFilePath):
    #     n_rem = 20

    response_var = "PtAvg"
    
    # Define the lower pt response values that are excluded from the curve fit.
    # 0 means no excluded bins, 1 means the first, 2 means the first 2, etc. 
    colors_fit_func = ['g','c','b','orange','r','y']
    fit_func_labels = [None]*len(colors_fit_func)
    data_labels = [None]*len(colors_fit_func)
    
    # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- 

    
    # Read the appropriate dataframe.
    df = pd.read_pickle(pickleFilePath)

    #f, ax = plt.subplots(figsize=(18.3*(1/2.54), 13.875*(1/2.54)))
    f, ax = plt.subplots(2)

    gs = gridspec.GridSpec(2, 1,height_ratios=[5,1])
    gs.update(wspace=0.05, hspace=0.05)              
    ax[0] = plt.subplot(gs[0])
    ax[1] = plt.subplot(gs[1])

    y_func_nom = None
    
    for param_name, i in zip(df.index, range(len(df.index))):

        print("Fitting responses of slice "+param_name)

        x=df["x"].loc[param_name]
        y=df["y"].loc[param_name]

        x_error=df["xError"].loc[param_name]
        y_error=df["yError"].loc[param_name]
        
        fit_func_labels[i] = "Fitted polylog "+param_name
        data_labels[i] = "GSC Responses "+param_name

        x = np.array(x)[:-n_rem]
        y = np.array(y)[:-n_rem]
        x_error = np.array(x_error)[:-n_rem]
        y_error = np.array(y_error)[:-n_rem]
        
        x_min = x[0] - x_error[0]
        x_max = x[-1] + x_error[-1]
        
        plot_range = np.linspace(x_min,x_max,1000)

        #ax.text(800,1.6,"eta = "+eta_range)
        ax[0].errorbar(x, y, yerr=y_error, xerr=x_error,
                    linestyle='None',
                    marker="o",
                    color=colors_fit_func[i],
                    markersize=2,
                    linewidth=0.5,
                    label=data_labels[i].replace("_"," "),
                )
        
        popt, pcov = None, None

        try:
            popt, pcov = curve_fit(polyLog, x, y, sigma=y_error)
        except RuntimeError,e:
            print(e.message)
            continue
        
        # popt_sigmin = np.copy(popt)
        # popt_sigmax = np.copy(popt)
        
        # for i in range(len(popt)):
        #     popt_sigmin[i] = popt[i] - 0.00001*(pcov[i,i]**0.5)
        #     popt_sigmax[i] = popt[i] + 0.00001*(pcov[i,i]**0.5)
        
        y_func = polyLog(plot_range, *popt)

        # y_sigup = polyLog(plot_range, *popt_sigmin)
        # y_sigdown = polyLog(plot_range, *popt_sigmax)
        y_func_ratio = None

        if (i == 0):
            y_func_nom = y_func
        
        y_func_ratio = y_func / y_func_nom

        ax[0].plot(plot_range, y_func, color=colors_fit_func[i], label=fit_func_labels[i].replace("_"," "))
        ax[1].plot(plot_range, y_func_ratio, color=colors_fit_func[i])
        # ax[0].plot(plot_range, y_func, color=colors_fit_func[i], label=fit_func_labels[i].replace("_"," "))
        # ax[1].plot(plot_range, y_func_ratio, color=colors_fit_func[i])
        # Set limits and labels
        ax[0].set_xlim(x_min, x_max)
        ax[1].set_xlim(x_min, x_max)
        ax[0].set_xscale('log')
        ax[0].set_ylim(0.97, 1.01)

        
        # if include_fituncert:
        #     ax.plot(plot_range, y_sigup, 'r--', label="Fitted function +- sigma*10e-5")
        #     ax.plot(plot_range, y_sigdown, 'r--')
        #     ax.fill_between(plot_range, y_sigup, y_sigdown, facecolor="gray", alpha=0.15)
    trigger_name = pickleFilePath[(pickleFilePath.rfind("j")-4):(pickleFilePath.rfind("j")+4)]

    try:
        int(trigger_name[-1])
    except ValueError:
        trigger_name = pickleFilePath[(pickleFilePath.rfind("j")-4):(pickleFilePath.rfind("j")+3)]

    ax[0].axvspan(getHLTPtAvgRange(trigger_name)[0], getHLTPtAvgRange(trigger_name)[1], facecolor='gray', alpha=0.5, label='Stitching Range')

    
    leg = ax[0].legend(borderpad=0.5, loc=1, ncol=2, frameon=True, prop={"size":10})#,framealpha=1)
    leg._legend_box.align = "left"
        #leg.set_title(plotDict[xAxisVariable]["legendTitle"])

    ax[0].text((float(getHLTPtAvgRange(trigger_name)[1]) - float(getHLTPtAvgRange(trigger_name)[0]))/2+float(getHLTPtAvgRange(trigger_name)[0]), 0.977, trigger_name.replace("_"," "), ha='center')
    ax[0].text((float(getHLTPtAvgRange(trigger_name)[1]) - float(getHLTPtAvgRange(trigger_name)[0]))/2+float(getHLTPtAvgRange(trigger_name)[0]), 0.975, "(0.99 Eff.)", ha='center')

    ax[1].ticklabel_format(axis='y',useOffset=False)
    ax[1].set_ylim(0.998, 1.002)

    # Set log scale
    ax[1].set_xscale('log')

    #plt.tick_params(axis='x', which='minor')
    ax[1].tick_params(axis='x', which='minor', labelsize = 12)
    ax[1].tick_params(axis='x', which='major', labelsize = 12)
    ax[1].tick_params(axis='y', labelsize=8)
    ax[1].xaxis.set_minor_formatter(FormatStrFormatter("%.0f"))
    ax[1].xaxis.set_major_formatter(FormatStrFormatter("%.0f"))

    ax[1].hlines(y=1.001, xmin=x_min, xmax=x_max, linewidth=1, linestyle='dashed', color='r')
    ax[1].hlines(y=0.999, xmin=x_min, xmax=x_max, linewidth=1, linestyle='dashed', color='r')

    # Set axis labels
    ax[0].set_ylabel("Online/Offline Response", fontsize=14, ha='right', y=0.7)    
    ax[0].get_xaxis().set_ticks([])
    
    ax[1].set_xlabel("$p_{T}^{avg}$", fontsize=14, ha='right',x=1.0)
    ax[1].set_ylabel(r'$\frac{N \sigma}{1.0 \sigma}$', fontsize=18, ha='right',y=0.8)
    #ax[1].set_ylabel(r'$\frac{\mbox{Polylog}}{\mbox{Response}}$', fontsize=8, ha='right',y=0.8)

    # Add grid
    ax[0].grid(True)
    
    # Add ATLAS label
    #hep.atlas.text("Internal",ax=ax)
    
    # Use tight layout
    #plt.tight_layout()

    pdf_path = outputPath+"response_fitted"+"_"+pickleFilePath[(pickleFilePath.rfind("/")+1):-7]+".pdf"
    
    # Save plot as .pdf
    f.savefig(pdf_path)
