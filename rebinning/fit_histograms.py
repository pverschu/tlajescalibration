

import pandas as pd
import numpy as np
import sys

sys.path.insert(1, '../Utils')

import pickle
from matplotlib.backends.backend_pdf import PdfPages
from JES_BalanceFitter import JES_BalanceFitter
from fit_functions import *
from helpers import *

import ROOT
-5

# -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- 

path="/afs/cern.ch/work/p/pverschu/private/LundSecondment/responseAndResolutionStudies"

# Define the list with root-files containing the 3D histograms.
listOfRootFilePaths = [#path+"/data/HLT_j175/merged.root",
                       path+"/data/v30/merged.root",
            ]

zvar = "etaLeading"
slicingAxis = "z"

yvar = "ptAvg"
projectionAxis = "y"

responseAxis = "x"

# (zmin, zmax, number of slices + 1)
zslices = np.linspace(-2.8,2.8,2)

projectionRebinWidths = [1,2]#,4,6]

nSigmaForFit = 1.
fitOptString = "RESQ"

Chi2ndof_min = 2.5

# Define some words that need to occur in the name of histograms in the root-files.
name_filters = ["GSC",zvar,yvar]

# -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- 

# Create JESBfitter object and set the fit options.
JESBfitter = JES_BalanceFitter(nSigmaForFit)
JESBfitter.SetGaus()
JESBfitter.SetFitOpt(fitOptString)


# Loop over the file paths
for rootFilePath in listOfRootFilePaths:

    print("Analyzing file "+rootFilePath)
    
    # Open root file containing 3D histograms
    inFile = ROOT.TFile.Open(rootFilePath) 

    # Get the histograms from the root-file that have all the 
    # key words in their object name.
    hist_names = get_list_hist_names(inFile, name_filters)

    # Loop over the histograms.
    for hist_name in hist_names:

        h3D = inFile.Get(hist_name)

        # Create a dictionary list from the key names in the ROOT file.
        dictionaryList = get_dict_list_rebinfactors(projectionRebinWidths)        

        # Create the dataframe form the dictionary list.
        df = pd.DataFrame(dictionaryList) 

        # Define a path and name for the dataframe
        dfPath = "pickles/"+hist_name+"_rebinFactors_"+str(projectionRebinWidths)

        # Loop over the slices
        for rebin_name, rebinFactor in zip(df.index, projectionRebinWidths):

            print("Rebinning the x axis with a factor of "+str(rebinFactor))
            
            # Get the 2D slice of the 3D histogram.
            h2D = get_2D_slice(inFile, hist_name, [zslices[0], zslices[1]], slicingAxis, responseAxis, projectionAxis, rebinFactor)
            
            # Loop over the bins in the 2D histogram x-axis
            for currentRebinnedBin in range(1, h2D.GetNbinsX()+1): # Histograms start at bin 1, plus one to include last bin

                pt_range = [h2D.GetXaxis().GetBinLowEdge(currentRebinnedBin), h2D.GetXaxis().GetBinUpEdge(currentRebinnedBin)]

                h1D = get_1D_slice(h2D, [zslices[0], zslices[1]], currentRebinnedBin)

                # If the current 2D bin is empty skip it
                if h1D.GetEntries() == 0:
                    #print("empty 1D hist, skipping!")
                    continue                

                # Set the fit limits based on the 1D histogram properties
                fitMax = h1D.GetMean() + nSigmaForFit * h1D.GetRMS()
                fitMin = h1D.GetMean() - nSigmaForFit * h1D.GetRMS()
                
                # Obtain fit using JES_BalanceFitter           
                JESBfitter.Fit(h1D, fitMin, fitMax)
                fit = JESBfitter.GetFit()
                histFit = JESBfitter.GetHisto()
                Chi2Ndof = JESBfitter.GetChi2Ndof()
                
                # if not math.isinf(Chi2Ndof) and Chi2Ndof > Chi2ndof_min:
                #     output_name = "response_fit_"+zvar+"_"+str(currentSlice)+"_"+yvar+"_"+str(pt_range)
                #     print("Plotting response histogram fit for "+zvar+"_"+str(list(zslices))+"_"+yvar+"_"+str(pt_range)+" with a Chi2/ndof of "+str(Chi2Ndof))
                #     plot_hist_fit(h1D, fit, output_name)

                # Initialize empty lists to hold the values of each 1D histogram.
                # There will be as many 1D histograms as Y axis bins of the 3D histogram
                # Get all the bin edges, entries and errors in python lists.
                binEdges, binEntries, binErrors = get_bin_edges_entries_errors(h1D)    

                df = fill_dataframe(df, rebin_name, h2D, fit, currentRebinnedBin, fitMin, fitMax, Chi2Ndof, binEdges, binEntries, binErrors)
        # Save the dataframe to disk as pickle. One dataframe per slice per root file path
        df.to_pickle(dfPath+".pickle") 
        del df

    inFile.Close() # Close the root file



# -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- # -- 
